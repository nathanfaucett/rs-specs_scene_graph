extern crate specs;
extern crate specs_bundler;
extern crate specs_scene_graph;

use specs::{
    WorldExt, Component, Builder, DenseVecStorage, DispatcherBuilder, Entity, FlaggedStorage, ReaderId, World,
};
use specs_bundler::Bundler;
use specs_scene_graph::{
    Event as SceneGraphEvent, Parent as SceneGraphParent, SceneGraph, SceneGraphBundle,
};

#[derive(Debug)]
struct Parent {
    pub entity: Entity,
}

impl Parent {
    #[inline(always)]
    pub fn new(entity: Entity) -> Self {
        Parent { entity: entity }
    }
}

impl Component for Parent {
    type Storage = FlaggedStorage<Self, DenseVecStorage<Self>>;
}

impl SceneGraphParent for Parent {
    #[inline(always)]
    fn parent_entity(&self) -> Entity {
        self.entity
    }
}

fn delete_removals(world: &mut World, reader_id: &mut ReaderId<SceneGraphEvent>) {
    let mut remove = vec![];

    for event in world
        .fetch::<SceneGraph<Parent>>()
        .changed()
        .read(reader_id)
    {
        if let SceneGraphEvent::Removed(entity) = *event {
            remove.push(entity);
        }
    }

    for entity in remove {
        if let Err(_) = world.delete_entity(entity) {
            println!("Failed removed entity");
        }
    }
}

#[test]
fn parent_removed() {
    let mut world = World::new();

    let mut dispatcher = Bundler::new(&mut world, DispatcherBuilder::new())
        .bundle(SceneGraphBundle::<Parent>::default())
        .unwrap()
        .build();

    let mut reader_id = world.write_resource::<SceneGraph<Parent>>().track();

    let entity0 = world.create_entity().build();
    let entity1 = world.create_entity().with(Parent::new(entity0)).build();
    let entity2 = world.create_entity().build();
    let entity3 = world.create_entity().with(Parent::new(entity2)).build();
    let entity4 = world.create_entity().with(Parent::new(entity3)).build();

    dispatcher.dispatch(&mut world);

    delete_removals(&mut world, &mut reader_id);
    world.maintain();

    let _ = world.delete_entity(entity0);
    dispatcher.dispatch(&mut world);
    delete_removals(&mut world, &mut reader_id);
    world.maintain();

    assert_eq!(world.is_alive(entity0), false);
    assert_eq!(world.is_alive(entity1), false);

    let _ = world.delete_entity(entity2);
    dispatcher.dispatch(&mut world);
    delete_removals(&mut world, &mut reader_id);
    world.maintain();

    assert_eq!(world.is_alive(entity2), false);
    assert_eq!(world.is_alive(entity3), false);
    assert_eq!(world.is_alive(entity4), false);

    assert_eq!(0, world.read_resource::<SceneGraph<Parent>>().all().len());
}
