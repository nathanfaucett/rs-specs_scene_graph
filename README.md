# specs_scene_graph

scene graph for specs

```rust
use specs_scene_graph::{SceneGraphBundle, Parent as SceneGraphParent};

#[derive(Debug)]
struct Parent {
    entity: Entity,
}

impl Parent {
    #[inline(always)]
    pub fn new(entity: Entity) -> Self {
        Parent { entity: entity }
    }
}

impl Component for Parent {
    type Storage = FlaggedStorage<Self, DenseVecStorage<Self>>;
}

impl SceneGraphParent for Parent {
    #[inline(always)]
    fn parent_entity(&self) -> Entity {
        self.entity
    }
}

let mut world = World::new();

let mut dispatcher = SpecsBundler::new(&mut world, DispatcherBuilder::new())
    .bundle(SceneGraphBundle::<Parent>::default())
    .unwrap()
    .build();

// track modified/removed events from the scene graph
let mut reader_id = world.write_resource::<SceneGraph<Parent>>().track();
```
