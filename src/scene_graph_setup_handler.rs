use std::marker::PhantomData;

use shred::SetupHandler;
use specs::{SystemData,World, Tracked, WriteStorage};

use super::{Parent, SceneGraph};

#[derive(Debug, Default)]
pub struct SceneGraphSetupHandler<P>(PhantomData<P>);

impl<P> SetupHandler<SceneGraph<P>> for SceneGraphSetupHandler<P>
where
    P: Parent,
    P::Storage: Default + Tracked,
{
    #[inline]
    fn setup(world: &mut World) {
        if !world.has_value::<SceneGraph<P>>() {
            let scene_graph = {
                let mut storage: WriteStorage<P> = SystemData::fetch(&world);
                SceneGraph::<P>::new(storage.register_reader())
            };

            world.insert(scene_graph);
        }
    }
}
