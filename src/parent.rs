use specs::{Component, Entity, Tracked};

pub trait Parent: 'static + Send + Sync + Component
where
    <Self as Component>::Storage: Default + Tracked,
{
    fn parent_entity(&self) -> Entity;
}
