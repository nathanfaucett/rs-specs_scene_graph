use specs::Entity;

#[derive(Debug)]
pub enum Event {
    Modified(Entity),
    Removed(Entity),
}
