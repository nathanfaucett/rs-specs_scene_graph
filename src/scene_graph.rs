use std::marker::PhantomData;

use fnv::{FnvHashMap, FnvHashSet};
use hibitset::{BitSet, BitSetLike};
use shrev::EventChannel;
use specs::prelude::ComponentEvent;
use specs::world::Index;
use specs::{Entities, Entity, Join, ReadStorage, ReaderId, Tracked};

use super::{Event, Parent};

pub struct SceneGraph<P> {
    sorted: Vec<Entity>,

    entities: FnvHashMap<Index, usize>,
    children: FnvHashMap<Entity, Vec<Entity>>,
    current_parent: FnvHashMap<Entity, Entity>,
    external_parents: FnvHashSet<Entity>,
    changed: EventChannel<Event>,

    reader_id: ReaderId<ComponentEvent>,

    modified: BitSet,
    inserted: BitSet,
    removed: BitSet,

    scratch_set: FnvHashSet<Entity>,

    _marker: PhantomData<P>,
}

impl<P> SceneGraph<P> {
    #[inline(always)]
    pub fn new(reader_id: ReaderId<ComponentEvent>) -> Self {
        SceneGraph {
            sorted: Vec::new(),

            entities: FnvHashMap::default(),
            children: FnvHashMap::default(),
            current_parent: FnvHashMap::default(),
            external_parents: FnvHashSet::default(),
            changed: EventChannel::default(),

            reader_id: reader_id,

            modified: BitSet::new(),
            inserted: BitSet::new(),
            removed: BitSet::new(),

            scratch_set: FnvHashSet::default(),

            _marker: PhantomData,
        }
    }

    #[inline]
    pub fn all(&self) -> &[Entity] {
        self.sorted.as_slice()
    }

    #[inline]
    pub fn children(&self, entity: Entity) -> &[Entity] {
        self.children
            .get(&entity)
            .map(|vec| vec.as_slice())
            .unwrap_or(&[])
    }
    #[inline]
    pub fn parent(&self, entity: Entity) -> Option<Entity> {
        self.current_parent.get(&entity).cloned()
    }

    #[inline(always)]
    pub fn track(&mut self) -> ReaderId<Event> {
        self.changed.register_reader()
    }
    #[inline(always)]
    pub fn changed(&self) -> &EventChannel<Event> {
        &self.changed
    }
}

impl<P> SceneGraph<P>
where
    P: Parent,
    P::Storage: Tracked + Default,
{
    #[inline(always)]
    pub fn maintain<'system>(
        &mut self,
        entities: Entities<'system>,
        parents: ReadStorage<'system, P>,
    ) {
        self.modified.clear();
        self.inserted.clear();
        self.removed.clear();

        let events = parents.channel().read(&mut self.reader_id);
        for event in events {
            match event {
                ComponentEvent::Modified(id) => {
                    self.modified.add(*id);
                }
                ComponentEvent::Inserted(id) => {
                    self.inserted.add(*id);
                }
                ComponentEvent::Removed(id) => {
                    self.removed.add(*id);
                }
            }
        }

        self.scratch_set.clear();

        for id in (&self.removed).iter() {
            if let Some(index) = self.entities.get(&id) {
                self.scratch_set.insert(self.sorted[*index]);
            }
        }
        for entity in &self.external_parents {
            if !entities.is_alive(*entity) {
                self.scratch_set.insert(*entity);
            }
        }

        if !self.scratch_set.is_empty() {
            let mut i = 0;
            let mut min_index = std::usize::MAX;
            while i < self.sorted.len() {
                let entity = self.sorted[i];
                let remove = self.scratch_set.contains(&entity)
                    || self
                        .current_parent
                        .get(&entity)
                        .map(|parent_entity| self.scratch_set.contains(&parent_entity))
                        .unwrap_or(false);
                if remove {
                    if i < min_index {
                        min_index = i;
                    }
                    self.scratch_set.insert(entity);
                    self.sorted.remove(i);
                    if let Some(children) = self
                        .current_parent
                        .get(&entity)
                        .cloned()
                        .and_then(|parent_entity| self.children.get_mut(&parent_entity))
                    {
                        if let Some(pos) = children.iter().position(|e| *e == entity) {
                            children.swap_remove(pos);
                        }
                    }
                    self.current_parent.remove(&entity);
                    self.children.remove(&entity);
                    self.entities.remove(&entity.id());
                } else {
                    i += 1;
                }
            }
            for i in min_index..self.sorted.len() {
                self.entities.insert(self.sorted[i].id(), i);
            }
            for entity in &self.scratch_set {
                self.changed.single_write(Event::Removed(*entity));
                self.external_parents.remove(entity);
            }
        }

        self.scratch_set.clear();
        for (entity, _, parent) in (&*entities, &self.inserted, &parents).join() {
            let parent_entity = parent.parent_entity();

            let insert_index = self
                .children
                .get(&entity)
                .and_then(|children| {
                    children
                        .iter()
                        .map(|child_entity| self.entities.get(&child_entity.id()).unwrap())
                        .min()
                        .cloned()
                })
                .unwrap_or(self.sorted.len());
            self.entities.insert(entity.id(), insert_index);
            if insert_index >= self.sorted.len() {
                self.sorted.push(entity);
            } else {
                self.sorted.insert(insert_index, entity);
                for i in insert_index..self.sorted.len() {
                    self.entities.insert(self.sorted[i].id(), i);
                }
            }

            {
                let children = self
                    .children
                    .entry(parent_entity)
                    .or_insert_with(Vec::default);
                children.push(entity);
            }

            self.current_parent.insert(entity, parent_entity);
            self.scratch_set.insert(entity);
            if !self.current_parent.contains_key(&parent_entity) {
                self.external_parents.insert(parent_entity);
            }
            self.external_parents.remove(&entity);
        }

        for (entity, _, parent) in (&*entities, &self.modified.clone(), &parents).join() {
            let parent_entity = parent.parent_entity();

            if let Some(old_parent) = self.current_parent.get(&entity).cloned() {
                if old_parent == parent_entity {
                    continue;
                }
                if let Some(children) = self.children.get_mut(&old_parent) {
                    if let Some(pos) = children.iter().position(|e| *e == entity) {
                        children.remove(pos);
                    }
                }
            }

            self.children
                .entry(parent_entity)
                .or_insert_with(Vec::default)
                .push(entity);

            let entity_index = self.entities.get(&entity.id()).cloned().unwrap();
            if let Some(parent_index) = self.entities.get(&parent_entity.id()).cloned() {
                let mut offset = 0;
                let mut process_index = parent_index;
                while process_index > entity_index {
                    let move_entity = self.sorted.remove(process_index);
                    self.sorted.insert(entity_index, move_entity);
                    offset += 1;
                    process_index = self
                        .current_parent
                        .get(&move_entity)
                        .and_then(|p_entity| self.entities.get(&p_entity.id()))
                        .map(|p_index| p_index + offset)
                        .unwrap_or(0);
                }

                if parent_index > entity_index {
                    for i in entity_index..parent_index {
                        self.entities.insert(self.sorted[i].id(), i);
                    }
                }
            }

            self.current_parent.insert(entity, parent_entity);
            self.scratch_set.insert(entity);

            if !self.current_parent.contains_key(&parent_entity) {
                self.external_parents.insert(parent_entity);
            }
        }

        if !self.scratch_set.is_empty() {
            for i in 0..self.sorted.len() {
                let entity = self.sorted[i];
                let notify = self.scratch_set.contains(&entity)
                    || self
                        .current_parent
                        .get(&entity)
                        .map(|parent_entity| self.scratch_set.contains(&parent_entity))
                        .unwrap_or(false);
                if notify {
                    self.scratch_set.insert(entity);
                    self.changed.single_write(Event::Modified(entity));
                }
            }
        }

        self.scratch_set.clear();
        for entity in &self.external_parents {
            if !self.children.contains_key(entity) {
                self.scratch_set.insert(*entity);
            }
        }
        for entity in &self.scratch_set {
            self.external_parents.remove(entity);
        }
    }
}
