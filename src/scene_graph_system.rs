use std::marker::PhantomData;

use specs::{Entities, ReadStorage, System, Tracked, Write};
use type_name;

use super::{Parent, SceneGraph, SceneGraphSetupHandler};

pub struct SceneGraphSystem<P>(PhantomData<P>);

impl<P> Default for SceneGraphSystem<P> {
    #[inline(always)]
    fn default() -> Self {
        SceneGraphSystem::new()
    }
}

impl<P> SceneGraphSystem<P> {
    #[inline(always)]
    pub fn new() -> Self {
        SceneGraphSystem(PhantomData)
    }
    #[inline]
    pub fn name() -> &'static str {
        type_name::get::<Self>()
    }
}

impl<'system, P> System<'system> for SceneGraphSystem<P>
where
    P: Parent,
    P::Storage: Default + Tracked,
{
    type SystemData = (
        Entities<'system>,
        ReadStorage<'system, P>,
        Write<'system, SceneGraph<P>, SceneGraphSetupHandler<P>>,
    );

    #[inline]
    fn run(&mut self, (entities, parents, mut scene_graph): Self::SystemData) {
        scene_graph.maintain(entities, parents);
    }
}
