use std::marker::PhantomData;

use specs::{System, Tracked, WorldExt};
use specs_bundler::{Bundle, Bundler};

use super::{Parent, SceneGraphSystem};

#[derive(Debug)]
pub struct SceneGraphBundle<'deps, P> {
    deps: Vec<&'deps str>,
    _marker: PhantomData<P>,
}

impl<'deps, P> Default for SceneGraphBundle<'deps, P> {
    #[inline(always)]
    fn default() -> Self {
        Self::new(&[])
    }
}

impl<'deps, P> SceneGraphBundle<'deps, P> {
    #[inline(always)]
    pub fn new(deps: &[&'deps str]) -> Self {
        SceneGraphBundle {
            deps: deps.to_vec(),
            _marker: PhantomData,
        }
    }
}

impl<'deps, 'world, 'a, 'b, P> Bundle<'world, 'a, 'b> for SceneGraphBundle<'deps, P>
where
    P: Parent,
    P::Storage: Default + Tracked,
{
    type Error = ();

    #[inline]
    fn bundle(
        self,
        mut bundler: Bundler<'world, 'a, 'b>,
    ) -> Result<Bundler<'world, 'a, 'b>, Self::Error> {
        bundler.world.register::<P>();

        let mut system = SceneGraphSystem::<P>::new();

        system.setup(&mut bundler.world);

        bundler.dispatcher_builder = bundler.dispatcher_builder.with(
            system,
            SceneGraphSystem::<P>::name(),
            self.deps.as_slice(),
        );

        Ok(bundler)
    }
}
